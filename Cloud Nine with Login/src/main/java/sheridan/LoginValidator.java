package sheridan;

public class LoginValidator {

	private int NUMBER_OF_CHARACTERS = 6;

	public static boolean isValidLoginName( String loginName ) {
		LoginValidator method = new LoginValidator();
		return method.hasEnoughCharacters(loginName) && method.hasNoSpecialCharacter(loginName);
	}
	
	public boolean hasNoSpecialCharacter(String input) {
		for(int i =0;i<input.length();i++) {	
			if(!(Character.isAlphabetic(input.charAt(i)) || Character.isDigit(input.charAt(i)))) {
				return false;
			}
		}
		return true;
	}
	
	public boolean hasEnoughCharacters(String input) {
		return input.length() > NUMBER_OF_CHARACTERS;
	}
}
