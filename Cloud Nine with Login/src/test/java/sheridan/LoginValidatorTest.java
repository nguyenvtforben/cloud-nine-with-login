package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	//No Special Character Test
	@Test
	public void testHasNoSpecialCharRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nguyenvt" ) );
	}

	@Test
	public void testHasNoSpecialCharException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "$nguyen_VSD!@" ) );
	}
	
	@Test
	public void testHasNoSpecialCharBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "HelloWorld" ) );
	}
	
	@Test
	public void testHasNoSpecialCharBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "Good Bye" ) );
	}
	
	//Log in name length test
	@Test
	public void testHasValidLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nguyen12" ) );
	}
	
	@Test
	public void testHasValidLengthException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "vt" ) );
	}
	
	@Test
	public void testHasValidLengthBoundaryIn( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "nguy3n1" ) );
	}
	
	@Test
	public void testHasValidLengthBoundaryOut( ) {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "h3ll00" ) );
	}
}
